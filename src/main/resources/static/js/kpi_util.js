const checkAutomationTest = function (item) {
    return item.kind === '자동';
};
const checkManualTest = function (item) {
    return item.kind === '수동';
};

const sortByKey = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

const groupBy = function (array, f) {
    var groups = {};
    array.forEach(function (o) {
        var group = JSON.stringify(f(o));
        groups[group] = groups[group] || [];
        groups[group].push(o);
    });

    return Object.keys(groups).map(function (group) {
        return groups[group];
    })
};

// 지난 주 월요일 부터 일요일 까지 필터
const checkLastWeek = function (item) {
    var curr = new Date;
    var first = curr.getDate() - curr.getDay() - 6;
    var last = first + 6;
    var startDate = new Date(curr.setDate(first));

    startDate = new Date(startDate.setHours(0, 0, 0, 0));
    var endDate = new Date(curr.setDate(last));
    endDate = new Date(endDate.setHours(23, 59, 59, 59));

    var executedDate = new Date(item.executed_date);

    if (executedDate >= startDate && executedDate <= endDate) {
        return true;
    }
}

// 서버에서 리턴해 준 데이터들의 형태를 맞춰준다.
const dataOrganizer = function (obj, sortKey) {
    var longestObjArray = [];
    var curLength = 0;
    var longestIdx = -1;

    for (var i = 0; i < Object.keys(obj).length; i++) {
        obj[Object.keys(obj)[i]] = sortByKey(obj[Object.keys(obj)[i]], sortKey);
        if (obj[Object.keys(obj)[i]].length > curLength) {
            curLength = obj[Object.keys(obj)[i]].length;
            longestIdx = i;
        }
    }

    // 가장 긴 배열 확인
    if (longestIdx !== -1) {
        longestObjArray = obj[Object.keys(obj)[longestIdx]];
    } else {
        throw '오류';
    }

    // 빈 부분 채워 넣기
    for (var i = 0; i < Object.keys(obj).length; i++) {
        if (i !== longestIdx) {
            for (var j = 0; j < longestObjArray.length; j++) {
                if (typeof obj[Object.keys(obj)[i]][j] === 'undefined' ||
                    longestObjArray[j][sortKey] !== obj[Object.keys(obj)[i]][j][sortKey]) {
                    var emptyObj = longestObjArray[j];
                    emptyObj.classification__count = 0;

                    obj[Object.keys(obj)[i]].splice(j, 0, emptyObj);
                }
            }
        }
    }

    return obj;
};

function generateData(labelArr, dataArr, bgColorArr) {

}
function registerPlugin() {
    Chart.pluginService.register({
        afterUpdate: function (chart) {
            if (chart.config.options.elements.arc.roundedCornersFor !== undefined) {
                var arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundedCornersFor];
                arc.round = {
                    x: (chart.chartArea.left + chart.chartArea.right) / 2,
                    y: (chart.chartArea.top + chart.chartArea.bottom) / 2,
                    radius: (chart.outerRadius + chart.innerRadius) / 2,
                    thickness: (chart.outerRadius - chart.innerRadius) / 2 - 1,
                    backgroundColor: arc._model.backgroundColor
                }
            }
        },

        afterDraw: function (chart) {
            if (chart.config.options.elements.arc.roundedCornersFor !== undefined) {
                var ctx = chart.chart.ctx;
                var arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundedCornersFor];
                var startAngle = Math.PI / 2 - arc._view.startAngle;
                var endAngle = Math.PI / 2 - arc._view.endAngle;

                ctx.save();
                ctx.translate(arc.round.x, arc.round.y);
                console.log(arc.round.startAngle)
                ctx.fillStyle = arc.round.backgroundColor;
                ctx.beginPath();
                ctx.arc(arc.round.radius * Math.sin(startAngle), arc.round.radius * Math.cos(startAngle), arc.round.thickness, 0, 2 * Math.PI);
                ctx.arc(arc.round.radius * Math.sin(endAngle), arc.round.radius * Math.cos(endAngle), arc.round.thickness, 0, 2 * Math.PI);
                ctx.closePath();
                ctx.fill();
                ctx.restore();
            }
        },
    });

    // write text plugin
    Chart.pluginService.register({
        afterUpdate: function (chart) {
            if (chart.config.options.elements.center) {
                var helpers = Chart.helpers;
                var centerConfig = chart.config.options.elements.center;
                var globalConfig = Chart.defaults.global;
                var ctx = chart.chart.ctx;

                var fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
                var fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);

                if (centerConfig.fontSize)
                    var fontSize = centerConfig.fontSize;
                // figure out the best font size, if one is not specified
                else {
                    ctx.save();
                    var fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
                    var maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
                    var maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);

                    do {
                        ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
                        var textWidth = ctx.measureText(maxText).width;

                        // check if it fits, is within configured limits and that we are not simply toggling back and forth
                        if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize)
                            fontSize += 1;
                        else {
                            // reverse last step
                            fontSize -= 1;
                            break;
                        }
                    } while (true)
                    ctx.restore();
                }

                // save properties
                chart.center = {
                    font: helpers.fontString(fontSize, fontStyle, fontFamily),
                    fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor)
                };
            }
        },
        afterDraw: function (chart) {
            if (chart.center) {
                var centerConfig = chart.config.options.elements.center;
                var ctx = chart.chart.ctx;

                ctx.save();
                ctx.font = chart.center.font;
                ctx.fillStyle = chart.center.fillStyle;
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
                var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
                ctx.fillText(centerConfig.text, centerX, centerY);
                ctx.restore();
            }
        },
    })
}