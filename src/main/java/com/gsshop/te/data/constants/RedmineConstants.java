package com.gsshop.te.data.constants;

/**
 * Created by binchoi on 2017. 3. 29..
 */
public class RedmineConstants {
    public static final Integer PROJECT_ID = 56; // 레드마인 프로젝트 고유번호
    public static final Integer TRACKER_NORMAL_ID = 21; // 유형 (일반)
    public static final Integer TRACKER_EMERGENCY_ID = 23; // 유형 (긴급)
    public static final Integer STATUS_ID = 29; // 상태 (개발중)
    public static final Integer PRIORITY_ID = 2; // 우선순위 (중)
    public static final Integer AUTHOR_ID = 1; // 등록자 (ADMIN)
    public static final String REDMINE_URL = "http://quality.gsshop.com/redmine";
    public static final String ISSUE_API = "/projects/itsm/issues.json";
    public static final String ATTACH_API = "/uploads.json";
    public static final String API_KEY_ADMIN = "683b2a6fd26d781a5f67bcaac6ae35ad540e8f2e";
    public static final String HEADER_KEY = "X-Redmine-API-Key";

    // 커스텀 필드 고유번호
    public static final Integer CSRNO_FIELD_ID = 5; // CSR NO
    public static final Integer CSR_REQ_FIELD_ID = 95; // CSR_요청분류
    public static final Integer REQ_USER_FIELD_ID = 44; // 요청자
    public static final Integer DEVELOPER_FIELD_ID = 93; // CSR_개발담당자
    public static final Integer MONITORING_FIELD_ID = 47; // 모니터링강화 여부
    public static final Integer SERV_NM = 94; // 업무 영역명
    public static final Integer CMPL_AGRE_DT = 70; // 배포예정일

}
