package com.gsshop.te.data.constants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by binchoi on 2017. 3. 29..
 */
public class RedmineCustomFieldGenerator {

    public static Map<String, Object> generateField(int fieldId, List<String> value) {
        Map<String, Object> field = new HashMap<>();
        field.put("id", fieldId);
        field.put("value", value);

        return field;
    }

    public static Map<String, Object> generateField(int fieldId, String value) {
        Map<String, Object> field = new HashMap<>();
        field.put("id", fieldId);
        field.put("value", value);

        return field;
    }

}
