package com.gsshop.te.data.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "root")
public class OrderDeliveryList {

    public OrderDeliveryList(){}
    private List<OrderDeliveryInfo> orderDeliveryInfoList;

    @XmlElementWrapper(name = "dataset")
    @XmlElement(name = "record")
    public List<OrderDeliveryInfo> getOrderDeliveryInfoList() {
        return orderDeliveryInfoList;
    }

    public void setOrderDeliveryInfoList(List<OrderDeliveryInfo> orderDeliveryInfoList) {
        this.orderDeliveryInfoList = orderDeliveryInfoList;
    }
}
