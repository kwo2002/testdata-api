package com.gsshop.te.data.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class OrderDeliveryInfo {

    private String attrPrdCd;
    private String chk;
    private String custAddrZipcd;
    private String delDtm;
    private String delRsnCntnt;
    private String delRsnNm;
    private String dlvDlvsCoCd;
    private String dlvInvNo;
    private String dlvPickMthodCd;
    private String dlvPickMthodDtlCd;
    private String dlvPickMthodNm;
    private String dtctCd;
    private String gtpDlvsCoCd;
    private String gtpInvNo;
    private String ordItemId;
    private String ordItemNo;
    private String ordNo;
    private String ordStCd;
    private String orgShippntCd;
    private String pageidx;
    private String prchTypCd;
    private String prdCd;
    private String prdNm;
    private String relsSchdDt;
    private String shipPickGbnCd;
    private String shippntCd;
    private String sordItemNo;
    private String sordNo;
    private String sordPriorRankCd;
    private String sordQty;
    private String sordStCd;
    private String supCd;
    private String supNm;
    private String totalcnt;

    public String getAttrPrdCd() {
        return attrPrdCd;
    }

    public void setAttrPrdCd(String attrPrdCd) {
        this.attrPrdCd = attrPrdCd;
    }

    public String getChk() {
        return chk;
    }

    public void setChk(String chk) {
        this.chk = chk;
    }

    public String getCustAddrZipcd() {
        return custAddrZipcd;
    }

    public void setCustAddrZipcd(String custAddrZipcd) {
        this.custAddrZipcd = custAddrZipcd;
    }

    public String getDelDtm() {
        return delDtm;
    }

    public void setDelDtm(String delDtm) {
        this.delDtm = delDtm;
    }

    public String getDelRsnCntnt() {
        return delRsnCntnt;
    }

    public void setDelRsnCntnt(String delRsnCntnt) {
        this.delRsnCntnt = delRsnCntnt;
    }

    public String getDelRsnNm() {
        return delRsnNm;
    }

    public void setDelRsnNm(String delRsnNm) {
        this.delRsnNm = delRsnNm;
    }

    public String getDlvDlvsCoCd() {
        return dlvDlvsCoCd;
    }

    public void setDlvDlvsCoCd(String dlvDlvsCoCd) {
        this.dlvDlvsCoCd = dlvDlvsCoCd;
    }

    public String getDlvInvNo() {
        return dlvInvNo;
    }

    public void setDlvInvNo(String dlvInvNo) {
        this.dlvInvNo = dlvInvNo;
    }

    public String getDlvPickMthodCd() {
        return dlvPickMthodCd;
    }

    public void setDlvPickMthodCd(String dlvPickMthodCd) {
        this.dlvPickMthodCd = dlvPickMthodCd;
    }

    public String getDlvPickMthodDtlCd() {
        return dlvPickMthodDtlCd;
    }

    public void setDlvPickMthodDtlCd(String dlvPickMthodDtlCd) {
        this.dlvPickMthodDtlCd = dlvPickMthodDtlCd;
    }

    public String getDlvPickMthodNm() {
        return dlvPickMthodNm;
    }

    public void setDlvPickMthodNm(String dlvPickMthodNm) {
        this.dlvPickMthodNm = dlvPickMthodNm;
    }

    public String getDtctCd() {
        return dtctCd;
    }

    public void setDtctCd(String dtctCd) {
        this.dtctCd = dtctCd;
    }

    public String getGtpDlvsCoCd() {
        return gtpDlvsCoCd;
    }

    public void setGtpDlvsCoCd(String gtpDlvsCoCd) {
        this.gtpDlvsCoCd = gtpDlvsCoCd;
    }

    public String getGtpInvNo() {
        return gtpInvNo;
    }

    public void setGtpInvNo(String gtpInvNo) {
        this.gtpInvNo = gtpInvNo;
    }

    public String getOrdItemId() {
        return ordItemId;
    }

    public void setOrdItemId(String ordItemId) {
        this.ordItemId = ordItemId;
    }

    public String getOrdItemNo() {
        return ordItemNo;
    }

    public void setOrdItemNo(String ordItemNo) {
        this.ordItemNo = ordItemNo;
    }

    public String getOrdNo() {
        return ordNo;
    }

    public void setOrdNo(String ordNo) {
        this.ordNo = ordNo;
    }

    public String getOrdStCd() {
        return ordStCd;
    }

    public void setOrdStCd(String ordStCd) {
        this.ordStCd = ordStCd;
    }

    public String getOrgShippntCd() {
        return orgShippntCd;
    }

    public void setOrgShippntCd(String orgShippntCd) {
        this.orgShippntCd = orgShippntCd;
    }

    public String getPageidx() {
        return pageidx;
    }

    public void setPageidx(String pageidx) {
        this.pageidx = pageidx;
    }

    public String getPrchTypCd() {
        return prchTypCd;
    }

    public void setPrchTypCd(String prchTypCd) {
        this.prchTypCd = prchTypCd;
    }

    public String getPrdCd() {
        return prdCd;
    }

    public void setPrdCd(String prdCd) {
        this.prdCd = prdCd;
    }

    public String getPrdNm() {
        return prdNm;
    }

    public void setPrdNm(String prdNm) {
        this.prdNm = prdNm;
    }

    public String getRelsSchdDt() {
        return relsSchdDt;
    }

    public void setRelsSchdDt(String relsSchdDt) {
        this.relsSchdDt = relsSchdDt;
    }

    public String getShipPickGbnCd() {
        return shipPickGbnCd;
    }

    public void setShipPickGbnCd(String shipPickGbnCd) {
        this.shipPickGbnCd = shipPickGbnCd;
    }

    public String getShippntCd() {
        return shippntCd;
    }

    public void setShippntCd(String shippntCd) {
        this.shippntCd = shippntCd;
    }

    public String getSordItemNo() {
        return sordItemNo;
    }

    public void setSordItemNo(String sordItemNo) {
        this.sordItemNo = sordItemNo;
    }

    public String getSordNo() {
        return sordNo;
    }

    public void setSordNo(String sordNo) {
        this.sordNo = sordNo;
    }

    public String getSordPriorRankCd() {
        return sordPriorRankCd;
    }

    public void setSordPriorRankCd(String sordPriorRankCd) {
        this.sordPriorRankCd = sordPriorRankCd;
    }

    public String getSordQty() {
        return sordQty;
    }

    public void setSordQty(String sordQty) {
        this.sordQty = sordQty;
    }

    public String getSordStCd() {
        return sordStCd;
    }

    public void setSordStCd(String sordStCd) {
        this.sordStCd = sordStCd;
    }

    public String getSupCd() {
        return supCd;
    }

    public void setSupCd(String supCd) {
        this.supCd = supCd;
    }

    public String getSupNm() {
        return supNm;
    }

    public void setSupNm(String supNm) {
        this.supNm = supNm;
    }

    public String getTotalcnt() {
        return totalcnt;
    }

    public void setTotalcnt(String totalcnt) {
        this.totalcnt = totalcnt;
    }
}
