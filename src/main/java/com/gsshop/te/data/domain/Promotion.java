package com.gsshop.te.data.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("프로모션")
public class Promotion {

    @ApiModelProperty(value = "프로모션 기본정보", required = true, position = 1)
    private PromotionMaster promotionMaster;

    @ApiModelProperty(value = "프로모션 상세정보", required = true, position = 2)
    private PromotionDetail promotionDetail;

    @ApiModelProperty(value = "등록자(사번)",example = "D2714", required = true, position = 0)
    private String regUser;

    public PromotionMaster getPromotionMaster() {
        return promotionMaster;
    }

    public void setPromotionMaster(PromotionMaster promotionMaster) {
        this.promotionMaster = promotionMaster;
    }

    public PromotionDetail getPromotionDetail() {
        return promotionDetail;
    }

    public void setPromotionDetail(PromotionDetail promotionDetail) {
        this.promotionDetail = promotionDetail;
    }

    public String getRegUser() {
        return regUser;
    }

    public void setRegUser(String regUser) {
        this.regUser = regUser;
    }

    @Override
    public String toString() {
        return "Promotion{" +
                "promotionMaster=" + promotionMaster +
                ", promotionDetail=" + promotionDetail +
                ", regUser='" + regUser + '\'' +
                '}';
    }
}
