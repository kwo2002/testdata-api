package com.gsshop.te.data.domain;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "API_HISTORY")
public class ApiHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer seq;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "api_seq")
    private ApiList apiList;

    private Integer status;
    private String regUser;

    private LocalDateTime regDate;

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public ApiList getApiList() {
        return apiList;
    }

    public void setApiList(ApiList apiList) {
        this.apiList = apiList;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRegUser() {
        return regUser;
    }

    public void setRegUser(String regUser) {
        this.regUser = regUser;
    }

    public LocalDateTime getRegDate() {
        return regDate;
    }

    public void setRegDate(LocalDateTime regDate) {
        this.regDate = regDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiHistory that = (ApiHistory) o;
        return Objects.equals(seq, that.seq);
    }

    @Override
    public int hashCode() {
        return Objects.hash(seq);
    }

    @Override
    public String toString() {
        return "ApiHistory{" +
                "seq=" + seq +
                ", apiList=" + apiList +
                ", status=" + status +
                ", regUser='" + regUser + '\'' +
                ", regDate=" + regDate +
                '}';
    }
}
