package com.gsshop.te.data.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@ApiModel("프로모션 상세정보")
public class PromotionDetail {

    @ApiModelProperty(value = "프로모션 고유번호", hidden = true)
    private String pmoSeq;

    @ApiModelProperty(value = "프로모션 수정여부 확인용 값", hidden = true)
    private String pmoChgVal;

    @ApiModelProperty(value = "프로모션 시작일", hidden = true)
    private LocalDateTime startDt;

    @ApiModelProperty(value = "프로모션 종료일", hidden = true)
    private LocalDateTime endDt;

    @ApiModelProperty(value = "프로모션 대상그룹", required = true)
    private List<PromotionTargetGroup> promotionTargetGroupList;

    public String getPmoSeq() {
        return pmoSeq;
    }

    public void setPmoSeq(String pmoSeq) {
        this.pmoSeq = pmoSeq;
    }

    public String getPmoChgVal() {
        return pmoChgVal;
    }

    public void setPmoChgVal(String pmoChgVal) {
        this.pmoChgVal = pmoChgVal;
    }

    public List<PromotionTargetGroup> getPromotionTargetGroupList() {
        return promotionTargetGroupList;
    }

    public void setPromotionTargetGroupList(List<PromotionTargetGroup> promotionTargetGroupList) {
        this.promotionTargetGroupList = promotionTargetGroupList;
    }

    public LocalDateTime getStartDt() {
        return startDt;
    }

    public void setStartDt(LocalDateTime startDt) {
        this.startDt = startDt;
    }

    public LocalDateTime getEndDt() {
        return endDt;
    }

    public String getStartDateFormat() {
        return startDt.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    public String getEndDateFormat() {
        return endDt.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    public void setEndDt(LocalDateTime endDt) {
        this.endDt = endDt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionDetail that = (PromotionDetail) o;
        return Objects.equals(pmoSeq, that.pmoSeq) &&
                Objects.equals(pmoChgVal, that.pmoChgVal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pmoSeq, pmoChgVal);
    }

    @Override
    public String toString() {
        return "PromotionDetail{" +
                "pmoSeq='" + pmoSeq + '\'' +
                ", pmoChgVal='" + pmoChgVal + '\'' +
                ", promotionTargetGroupList=" + promotionTargetGroupList +
                '}';
    }
}
