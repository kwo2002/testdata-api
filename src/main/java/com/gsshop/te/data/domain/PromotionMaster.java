package com.gsshop.te.data.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@ApiModel("프로모션 기본정보")
public class PromotionMaster {

    @ApiModelProperty(value = "프로모션 제목", required = true, example = "프로모션제목")
    private String pmoNm;

    @ApiModelProperty(value = "프로모션설명 (등록자)", required = true, position = 1, example = "윤민철")
    private String pmoDescr;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @ApiModelProperty(value = "프로모션 시작일", example = "2017-09-30T15:38:08", position = 2)
    private LocalDateTime startDate = LocalDateTime.now();

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @ApiModelProperty(value = "프로모션 종료일", example = "2017-10-01T15:38:08", position = 3)
    private LocalDateTime endDate = startDate.plusWeeks(1);

    @ApiModelProperty(hidden = true)
    private String pmoSeq;
    @ApiModelProperty(hidden = true)
    private String pmoNeoChgVal;

    public String getPmoSeq() {
        return pmoSeq;
    }

    public void setPmoSeq(String pmoSeq) {
        this.pmoSeq = pmoSeq;
    }

    public String getPmoNm() {
        return pmoNm;
    }

    public void setPmoNm(String pmoNm) {
        this.pmoNm = pmoNm;
    }

    public String getPmoDescr() {
        return pmoDescr;
    }

    public void setPmoDescr(String pmoDescr) {
        this.pmoDescr = pmoDescr;
    }

    public String getPmoNeoChgVal() {
        return pmoNeoChgVal;
    }

    public void setPmoNeoChgVal(String pmoNeoChgVal) {
        this.pmoNeoChgVal = pmoNeoChgVal;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getStartDateFormat() {
        return startDate.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    public String getEndDateFormat() {
        return endDate.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionMaster that = (PromotionMaster) o;
        return Objects.equals(pmoSeq, that.pmoSeq);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pmoSeq);
    }

    @Override
    public String toString() {
        return "PromotionMaster{" +
                "pmoNm='" + pmoNm + '\'' +
                ", pmoDescr='" + pmoDescr + '\'' +
                ", pmoSeq='" + pmoSeq + '\'' +
                ", pmoNeoChgVal='" + pmoNeoChgVal + '\'' +
                '}';
    }
}
