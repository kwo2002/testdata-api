package com.gsshop.te.data.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Objects;

@ApiModel("프로모션 상세정보 고객그룹")
public class PromotionTargetGroup {

    @ApiModelProperty(value = "롯데: AX, 비씨: BC, 현대: DA, KEB하나: HN, 국민: KB, 신한: LG, 농협: NH, 삼성: SS", example = "LG", allowableValues = "AX, BC, DA, HN, KM, LG, NH, SS", required = true)
    private String cardCoCd;

    @ApiModelProperty(value = "프로모션 적용: N 프로모션 제외: Y", required = true, allowableValues = "Y, N", example = "N")
    private String pmoApplyExcptYn;

    @ApiModelProperty(value = "상품 번호", required = true)
    private List<String> prdCdList;

    @ApiModelProperty(value = "할인율", example = "5")
    private String chrDcRt = "5";

    @ApiModelProperty(hidden = true)
    private String minPayAmt;
    @ApiModelProperty(hidden = true)
    private String custGrpNm;

    public String getCardCoCd() {
        return cardCoCd;
    }

    public void setCardCoCd(String cardCoCd) {
        this.cardCoCd = cardCoCd;
    }

    public String getPmoApplyExcptYn() {
        return pmoApplyExcptYn;
    }

    public void setPmoApplyExcptYn(String pmoApplyExcptYn) {
        this.pmoApplyExcptYn = pmoApplyExcptYn;
    }

    public List<String> getPrdCdList() {
        return prdCdList;
    }

    public void setPrdCdList(List<String> prdCdList) {
        this.prdCdList = prdCdList;
    }

    public String getChrDcRt() {
        return chrDcRt;
    }

    public void setChrDcRt(String chrDcRt) {
        this.chrDcRt = chrDcRt;
    }

    public String getMinPayAmt() {
        return minPayAmt;
    }

    public void setMinPayAmt(String minPayAmt) {
        this.minPayAmt = minPayAmt;
    }

    public String getCustGrpNm() {
        return custGrpNm;
    }

    public void setCustGrpNm(String custGrpNm) {
        this.custGrpNm = custGrpNm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionTargetGroup that = (PromotionTargetGroup) o;
        return Objects.equals(cardCoCd, that.cardCoCd) &&
                Objects.equals(pmoApplyExcptYn, that.pmoApplyExcptYn) &&
                Objects.equals(prdCdList, that.prdCdList) &&
                Objects.equals(chrDcRt, that.chrDcRt) &&
                Objects.equals(minPayAmt, that.minPayAmt) &&
                Objects.equals(custGrpNm, that.custGrpNm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardCoCd, pmoApplyExcptYn, prdCdList, chrDcRt, minPayAmt, custGrpNm);
    }

    @Override
    public String toString() {
        return "PromotionTargetGroup{" +
                "cardCoCd='" + cardCoCd + '\'' +
                ", pmoApplyExcptYn='" + pmoApplyExcptYn + '\'' +
                ", prdCdList=" + prdCdList +
                ", chrDcRt='" + chrDcRt + '\'' +
                ", minPayAmt='" + minPayAmt + '\'' +
                ", custGrpNm='" + custGrpNm + '\'' +
                '}';
    }
}
