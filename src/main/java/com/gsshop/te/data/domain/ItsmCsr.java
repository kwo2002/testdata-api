package com.gsshop.te.data.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "ITSM_CSR")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItsmCsr {
    @Id
    private String srId;
    private String qusrNm;
    private String reqCl;
    private String eusrNm;
    private String developers;

    private String srTitlNm;
    private String srCn;
    private String servNm;
    private String emerRt;
    private String fncRiskYn;
    private LocalDate reqDt;
    private LocalDateTime chgCmplAgreDt;

    public String getSrId() {
        return srId;
    }

    public void setSrId(String srId) {
        this.srId = srId;
    }

    public String getQusrNm() {
        return qusrNm;
    }

    public void setQusrNm(String qusrNm) {
        this.qusrNm = qusrNm;
    }

    public String getReqCl() {
        return reqCl;
    }

    public void setReqCl(String reqCl) {
        this.reqCl = reqCl;
    }

    public String getEusrNm() {
        return eusrNm;
    }

    public void setEusrNm(String eusrNm) {
        this.eusrNm = eusrNm;
    }

    public String getDevelopers() {
        return developers;
    }

    public void setDevelopers(String developers) {
        this.developers = developers;
    }

    public String getSrTitlNm() {
        return srTitlNm;
    }

    public void setSrTitlNm(String srTitlNm) {
        this.srTitlNm = srTitlNm;
    }

    public String getSrCn() {
        return srCn;
    }

    public void setSrCn(String srCn) {
        this.srCn = srCn;
    }

    public String getServNm() {
        return servNm;
    }

    public String getEmerRt() {
        return emerRt;
    }

    public void setEmerRt(String emerRt) {
        this.emerRt = emerRt;
    }

    public String getFncRiskYn() {
        return fncRiskYn;
    }

    public void setFncRiskYn(String fncRiskYn) {
        this.fncRiskYn = fncRiskYn;
    }

    public void setServNm(String servNm) {
        this.servNm = servNm;
    }

    public LocalDate getReqDt() {
        return reqDt;
    }

    public void setReqDt(LocalDate reqDt) {
        this.reqDt = reqDt;
    }

    public LocalDateTime getChgCmplAgreDt() {
        return chgCmplAgreDt;
    }

    public void setChgCmplAgreDt(LocalDateTime chgCmplAgreDt) {
        this.chgCmplAgreDt = chgCmplAgreDt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItsmCsr itsmCsr = (ItsmCsr) o;
        return Objects.equals(srId, itsmCsr.srId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(srId);
    }

    @Override
    public String toString() {
        return "ItsmCsr{" +
                "srId='" + srId + '\'' +
                ", srTitlNm='" + srTitlNm + '\'' +
                ", servNm='" + servNm + '\'' +
                '}';
    }
}
