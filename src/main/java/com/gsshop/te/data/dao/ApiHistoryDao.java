package com.gsshop.te.data.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface ApiHistoryDao {
    List<Map<String, Object>> selectCallCountPerApi(LocalDateTime begin, LocalDateTime end);

    List<Map<String, Object>> selectBest5Caller();
}
