package com.gsshop.te.data.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ApiHistoryDaoImpl implements ApiHistoryDao {
    @Autowired
    private NamedParameterJdbcTemplate template;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Override
    public List<Map<String, Object>> selectCallCountPerApi(LocalDateTime begin, LocalDateTime end) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT name, count(a.api_seq) AS count, b.url, b.description FROM api_history a INNER JOIN api_list b ON a.api_seq = b.seq ");
        sql.append("WHERE to_char(a.reg_date, 'yyyy-MM-dd') BETWEEN :begin AND :end ");
        sql.append("GROUP BY b.name, a.api_seq, b.url, b.description ");
        sql.append("ORDER BY count DESC ");
        sql.append("LIMIT 3 ");

        Map<String, String> paramMap = new HashMap<>();

        paramMap.put("begin", begin.format(formatter));
        paramMap.put("end", end.format(formatter));

        return this.template.queryForList(sql.toString(), paramMap);
    }

    @Override
    public List<Map<String, Object>> selectBest5Caller() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        sql.append("        reg_user, ");
        sql.append("count(reg_user) as cnt ");
        sql.append("FROM api_history a INNER JOIN api_list b ON a.api_seq = b.seq ");
        sql.append("GROUP BY reg_user ");
        sql.append("ORDER BY cnt DESC ");
        sql.append("LIMIT 5 ");

        return this.jdbcTemplate.queryForList(sql.toString());
    }

}
