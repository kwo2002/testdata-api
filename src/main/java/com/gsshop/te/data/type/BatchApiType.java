package com.gsshop.te.data.type;

public enum BatchApiType {
    // 프로모션 배치
    GET_PROMOTION_BATCH("http://10.52.221.31:8080/scheduler/batch.scheduler.execlog.JobGroupExecOrderSend.dev?executorId=SCHEDULER-EMBEDDED&groupId=PMOBT0002&timestamp=20170901111012.00"),

    // 주문상태 변경: 배송준비중 배치 1
    DLV_READY_BATCH_1("http://10.52.221.40:8180/scheduler/batch.scheduler.execlog.JobGroupExecOrderSend.dev?executorId=SCHEDULER-EMBEDDED&groupId=DTRBT0189&timestamp=20171017112922.84"),
    // 주문상태 변경: 배송준비중 배치 2
    DLV_READY_BATCH_2("http://10.52.221.40:8180/scheduler/batch.scheduler.execlog.JobGroupExecOrderSend.dev?executorId=SCHEDULER-EMBEDDED&groupId=DTRBT0302&timestamp=20171017113654.40");

    private String url;

    BatchApiType(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}
