package com.gsshop.te.data.type;

public enum SalesOneDlvApiType {
    POST_SEARCH_ORDER("/mip/ordByDstrbInfoMod/getOrdByDstrbInfoModList/10/rspService.gs"),
    POST_SAVE_ORD("/mip/ordByDstrbInfoMod/saveOrdByDstrbInfoModList/10/rspService.gs"),
    POST_FORCE_SHIPPING("/mip/noRelsMng/handleCmpulRelsFsh/10/rspService.gs"),
    POST_FORCE_SHIP_COMPLETED("/mip/noRelsMng/handleCmpulDlvFsh/10/rspService.gs");


    private String url;
    SalesOneDlvApiType(String url) {
        this.url = url;
    }

    public String getUrl() {
        String salesoneUrl = "http://10.52.221.38:8080";
        return salesoneUrl + this.url;
    }
}
