package com.gsshop.te.data.type;

public enum CardType {
    AX("롯데카드"), BC("BC카드"), DA("현대카드"), HN("KEB하나"), KM("KB국민카드"), LG("신한카드"), NH("농협카드"), SS("삼성카드");

    private String cardName;

    CardType(String cardName) {
        this.cardName = cardName;
    }

    public String getCardName() {
        return this.cardName;
    }
}
