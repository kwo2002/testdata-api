package com.gsshop.te.data.type;

public enum SalesOnePromotionApiType {
    POST_SAVE_MASTER("/mip/gsMktPmoMng/savePmoBaseInfo/10/rspService.gs"),
    POST_SAVE_DETAIL("/mip/gsMktPmoMng/savePmoTgtGrpInfo/10/rspService.gs"),
    POST_SEARCH_PRODUCT("/mip/prdQryPopup/getPrdList/10/rspService.gs"),
    POST_GENERATE_SEQUENCE("/mip/gsMktPmoMng/getPmoSequence/10/rspService.gs"),
    POST_UPDATE_STATUS("/mip/gsMktPmoMng/changePmoStatus/10/rspService.gs");

    private String url;
    SalesOnePromotionApiType(String url) {
        this.url = url;
    }

    public String getUrl() {
        String salesoneUrl = "http://10.52.221.38:8080";
        return salesoneUrl + this.url;
    }
}
