package com.gsshop.te.data.type;

public enum PromotionStatus {
    REQUEST("12"), // 승인요청
    MODIFY("20"), // 승인후수정
    MODIFY_REQUEST("22"), // [수정]승인요청
    APPROVAL("13"),
    REJECT("29"); // 반려

    private String stCd;

    PromotionStatus(String stCd) {
        this.stCd = stCd;
    }

    public String getStCd() {
        return this.stCd;
    }
}
