package com.gsshop.te.data.type;

public enum  OrderStatusType {
    PAYMENT_COMPLETED("21"), // 입금완료
    WEHN_SHIPPED("22"); //출하지시

    private String statusCode;

    OrderStatusType(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }
}
