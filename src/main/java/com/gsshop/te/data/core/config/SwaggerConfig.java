package com.gsshop.te.data.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.collect.Lists.newArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public UiConfiguration uiConfig() {
        return new UiConfiguration(
                null, "list", "alpha", "schema", UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS, false, true, null
        );
    }
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("com.gsshop.te.data.controller"))
                    .build()
                .apiInfo(apiInfo())
                .pathMapping("/")
                .globalResponseMessage(RequestMethod.POST,
                        newArrayList(
                                new ResponseMessageBuilder()
                                    .code(500)
                                    .message("알 수 없는 오류가 발생하였습니다. 관리자에게 문의하세요.")
                                    .build(),
                                new ResponseMessageBuilder()
                                    .code(400)
                                    .message("외부 API 는 성공적으로 호출하였으나, 호출결과값이 실패 입니다. 요청 데이터의 유효성을 검증하세요.")
                                    .build(),
                                new ResponseMessageBuilder()
                                    .code(502)
                                    .message("외부 API 서버의 호출에 실패하였습니다.")
                                    .build(),
                                new ResponseMessageBuilder()
                                    .code(201)
                                    .message("성공적으로 저장하였습니다.")
                                    .build()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("테스트 데이터 관리 API Document")
                .description("통테환경의 테스트 데이터를 편리하게 생성 및 관리 할 수 있도록 지원하는 API 입니다.")
                .version("1.0.0")
                .build();
    }
}
