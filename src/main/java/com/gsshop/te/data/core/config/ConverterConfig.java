package com.gsshop.te.data.core.config;

import com.gsshop.te.data.domain.OrderDeliveryInfo;
import com.gsshop.te.data.domain.OrderDeliveryList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class ConverterConfig {

    /**
     * @return the RetTemplate Object. This method sets the marshaller object as
     *         params to RestTemplate leveraging the marshalling capability to
     *         the template.
     */
    @Bean(name = "restTemplate")
    public RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();

        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(getMarshallingHttpMessageConverter());
        converters.add(new FormHttpMessageConverter());
        converters.add(new StringHttpMessageConverter());
        restTemplate.setMessageConverters(converters);
        return restTemplate;
    }

    /**
     * @return MarshallingHttpMessageConverter object which is responsible for
     *         marshalling and unMarshalling process
     */
    @Bean(name = "marshallingHttpMessageConverter")
    public MarshallingHttpMessageConverter getMarshallingHttpMessageConverter() {

        MarshallingHttpMessageConverter marshallingHttpMessageConverter = new MarshallingHttpMessageConverter();
        marshallingHttpMessageConverter.setMarshaller(getJaxb2Marshaller());
        marshallingHttpMessageConverter.setUnmarshaller(getJaxb2Marshaller());
        return marshallingHttpMessageConverter;
    }

    /**
     * @return Jaxb2Marshaller, this is the Jaxb2Marshaller object
     */
    @Bean(name = "jaxb2Marshaller")
    public Jaxb2Marshaller getJaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setPackagesToScan("com.gsshop.te.data.domain");
        return jaxb2Marshaller;
    }
}
