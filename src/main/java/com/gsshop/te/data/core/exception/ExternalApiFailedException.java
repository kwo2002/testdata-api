package com.gsshop.te.data.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "API 는 성공적으로 호출하였으나, 호출 결과가 실패하였습니다. 요청 데이터의 유효성을 검증하세요.")
public class ExternalApiFailedException extends RuntimeException {
    public ExternalApiFailedException(String message) {
        super(message);
    }
}
