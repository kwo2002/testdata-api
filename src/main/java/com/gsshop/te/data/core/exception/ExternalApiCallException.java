package com.gsshop.te.data.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_GATEWAY, reason = "외부 API 호출에 실패하였습니다.")
public class ExternalApiCallException extends RuntimeException {
    public ExternalApiCallException(String message) {
        super(message);
    }
}
