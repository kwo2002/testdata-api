package com.gsshop.te.data.core.advice;

import com.gsshop.te.data.core.exception.ExternalApiCallException;
import com.gsshop.te.data.core.exception.ExternalApiFailedException;
import com.gsshop.te.data.domain.ApiHistory;
import com.gsshop.te.data.service.ApiHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionAdvice.class);

    @Autowired
    private ApiHistoryService apiHistoryService;

    @ExceptionHandler(value = ExternalApiCallException.class)
    @ResponseStatus(value = HttpStatus.BAD_GATEWAY, reason = "외부 API 호출에 실패하였습니다.")
    public void externalApiCall(ExternalApiCallException e, HttpServletRequest request) {
        logger.error(e.getMessage());
        saveHistory(request);
    }

    @ExceptionHandler(value = ExternalApiFailedException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "API 는 성공적으로 호출하였으나, 호출 결과가 실패하였습니다. 요청 데이터의 유효성을 검증하세요.")
    public void externalApiFailed(ExternalApiCallException e, HttpServletRequest request) {
        logger.error(e.getMessage());
        saveHistory(request);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "알 수 없는 오류가 발생하였습니다. 관리자에게 문의하세요.")
    public void handleException(Exception e, HttpServletRequest request) {
        e.printStackTrace();
        logger.error(e.getMessage());
        saveHistory(request);
    }

    private void saveHistory(HttpServletRequest request) {
        String regUser = request.getParameter("regUser");
        String uri = request.getRequestURI();
        String method = request.getMethod();

        this.apiHistoryService.saveHistory(uri, method, regUser);
    }
}
