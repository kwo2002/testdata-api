package com.gsshop.te.data.repository;

import com.gsshop.te.data.domain.ApiList;
import com.gsshop.te.data.type.HttpMethod;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface ApiListRepository extends JpaRepository<ApiList, Integer> {
    ApiList findByUrlAndMethod(String url, String method);

    List<ApiList> findByRegDateBetween(LocalDateTime begin, LocalDateTime end);
}
