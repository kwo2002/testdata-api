package com.gsshop.te.data.repository;

import com.gsshop.te.data.domain.ApiHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface ApiHistoryRepository extends JpaRepository<ApiHistory, Integer> {
    List<ApiHistory> findByRegDateBetweenOrderByRegDateDesc(LocalDateTime begin, LocalDateTime end);
}
