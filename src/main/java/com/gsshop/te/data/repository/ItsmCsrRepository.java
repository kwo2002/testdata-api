package com.gsshop.te.data.repository;

import com.gsshop.te.data.domain.ItsmCsr;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface ItsmCsrRepository extends JpaRepository<ItsmCsr, String> {
    List<ItsmCsr> findByReqDtEquals(LocalDate reqDt);
}
