package com.gsshop.te.data.service;

import com.gsshop.te.data.domain.ItsmCsr;

import java.util.List;

public interface ItsmService {
    List<ItsmCsr> getCsrList();

    List<ItsmCsr> insertCsrList(List<ItsmCsr> csrList);

    void saveRedmine(List<ItsmCsr> csrList);
}
