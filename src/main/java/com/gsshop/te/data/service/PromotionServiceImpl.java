package com.gsshop.te.data.service;

import com.gsshop.te.data.core.exception.ExternalApiFailedException;
import com.gsshop.te.data.domain.PromotionDetail;
import com.gsshop.te.data.domain.PromotionMaster;
import com.gsshop.te.data.domain.PromotionTargetGroup;
import com.gsshop.te.data.type.BatchApiType;
import com.gsshop.te.data.type.CardType;
import com.gsshop.te.data.type.PromotionStatus;
import com.gsshop.te.data.type.SalesOnePromotionApiType;
import com.gsshop.te.data.util.FileReadUtil;
import com.gsshop.te.data.util.HttpUtil;
import com.gsshop.te.data.util.SalesoneUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static java.util.stream.Collectors.joining;
import static org.springframework.util.StringUtils.delete;
import static org.springframework.util.StringUtils.replace;

@Service
public class PromotionServiceImpl implements PromotionService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 프로모션 기본정보 저장
     *
     * @param promotionMaster
     * @return pmoSeq 프로모션 Sequence (PK)
     */
    @Override
    public Map<String, String> saveMaster(PromotionMaster promotionMaster) throws Exception {
        String pmoMasterXml = FileReadUtil.readAll("salesone_request/savePmoMaster.xml");
        pmoMasterXml = replace(pmoMasterXml, "#{pmoSeq}", promotionMaster.getPmoSeq());
        pmoMasterXml = replace(pmoMasterXml, "#{pmoNm}", promotionMaster.getPmoNm());
        pmoMasterXml = replace(pmoMasterXml, "#{pmoDescr}", promotionMaster.getPmoDescr());
        pmoMasterXml = replace(pmoMasterXml, "#{pmoModDt}", promotionMaster.getStartDateFormat());
        pmoMasterXml = replace(pmoMasterXml, "#{pmoEndDt}", promotionMaster.getEndDateFormat());
        pmoMasterXml = replace(pmoMasterXml, "#{pmoStartDt}", promotionMaster.getStartDateFormat());
        pmoMasterXml = replace(pmoMasterXml, "#{sysDate}", promotionMaster.getStartDateFormat());
        pmoMasterXml = replace(pmoMasterXml, "#{pmoNeoChgVal}", promotionMaster.getPmoNeoChgVal());

        ResponseEntity<Map> response = HttpUtil.post(pmoMasterXml, SalesOnePromotionApiType.POST_SAVE_MASTER.getUrl(), Map.class);
        Map<String, String> result = SalesoneUtil.generateApiResult(response, "Promotion 기본정보 저장");

        result.put("pmoChgVal", promotionMaster.getPmoNeoChgVal());
        return result;
    }

    /**
     * 프로모션 상세(대상고객그룹, 혜택, 카드) 정보 저장
     * @param promotionDetail
     */
    @Override
    public Map<String, String> saveDetail(PromotionDetail promotionDetail) throws Exception {
        Map<String, String> result = new HashMap<>();

        String pmoChgVal = promotionDetail.getPmoChgVal();
        List<PromotionTargetGroup> promotionTargetGroupList = promotionDetail.getPromotionTargetGroupList();
        List<String> collection = validateProductCd(promotionTargetGroupList);

        if (!CollectionUtils.isEmpty(collection)) {
            String invalidPrdCd = collection.stream().collect(joining(","));
            logger.error("유효하지 않은 상품코드가 요청되었습니다. {}", invalidPrdCd);
            throw new ExternalApiFailedException("유효하지 않은 상품코드가 요청되었습니다. " + invalidPrdCd);
        }

        for (PromotionTargetGroup promotionTargetGroup : promotionTargetGroupList) {
            promotionDetail.setPmoChgVal(pmoChgVal);
            String promotionDetailXml = generatePromotionDetailXml(promotionDetail, promotionTargetGroup);

            ResponseEntity<Map> response = HttpUtil.post(promotionDetailXml, SalesOnePromotionApiType.POST_SAVE_DETAIL.getUrl(), Map.class);
            result = SalesoneUtil.generateApiResult(response, "대상고객그룹/혜택/카드 저장");
            pmoChgVal = result.get("pmoChgVal");
        }

        return result;
    }

    @Override
    public List<String> validateProductCd(List<PromotionTargetGroup> promotionTargetGroupList) throws Exception {
        List<String> wrongPrdCdList = new ArrayList<>();
        for (PromotionTargetGroup promotionTargetGroup : promotionTargetGroupList) {
            wrongPrdCdList.addAll(filterNotExistProductCd(promotionTargetGroup.getPrdCdList()));
        }

        return wrongPrdCdList;
    }

    /**
     * 프로모션 상세 xml 생성
     *
     * @param promotionDetail
     * @param promotionTargetGroup 대상고객그룹 (혜택, 대상상품)
     * @return
     * @throws Exception
     */
    private String generatePromotionDetailXml(PromotionDetail promotionDetail, PromotionTargetGroup promotionTargetGroup) throws Exception {
        Map<String, String> targetGrpXmlMap = generateTargetGroupXml(promotionDetail, promotionTargetGroup);

        String promotionDetailXml = generateTargetOrExcludeProductXml(promotionTargetGroup, targetGrpXmlMap.get("applyTargetProductXml"));

        promotionDetailXml = replace(promotionDetailXml, "#{targetGroup}", targetGrpXmlMap.get("targetGroupXml"));
        promotionDetailXml = replace(promotionDetailXml, "#{benefitCardChargeDc}", targetGrpXmlMap.get("benefitCardChargeXml"));
        promotionDetailXml = replace(promotionDetailXml, "#{benefitCardImmDc}", targetGrpXmlMap.get("benefitCardChargeXml"));

        return promotionDetailXml;
    }

    /**
     * 대상고객그룹 xml 생성 (대상상품, 혜택정보 xml) 생성
     *
     * @param promotionDetail
     * @param promotionTargetGroup
     * @return
     * @throws Exception
     */
    private Map<String, String> generateTargetGroupXml(PromotionDetail promotionDetail, PromotionTargetGroup promotionTargetGroup) throws Exception {
        Map<String, String> detailXmlMap = new HashMap<>();
        detailXmlMap.put("applyTargetProductXml", generateApplyTargetProductXml(promotionDetail, promotionTargetGroup));
        detailXmlMap.put("benefitCardChargeXml", generateBenefitCardXml(promotionDetail, promotionTargetGroup));
        detailXmlMap.put("targetGroupXml", generateTargetGroup(promotionDetail.getPmoSeq(), promotionDetail.getPmoChgVal()));

        return detailXmlMap;
    }

    /**
     * 프로모션 대상, 제외 구분에 따른 xml
     * @param promotionTargetGroup
     * @param applyTargetProductXml
     * @return
     */
    private String generateTargetOrExcludeProductXml(PromotionTargetGroup promotionTargetGroup, String applyTargetProductXml) {
        String promotionDetailXml = FileReadUtil.readAll("salesone_request/savePmoDetail.xml");
        if (isExceptProduct(promotionTargetGroup)) {
            applyTargetProductXml = delete(applyTargetProductXml, "<dealNo/>\r\n");
            promotionDetailXml = delete(promotionDetailXml, "#{applyTargetProduct}\r\n");
            promotionDetailXml = replace(promotionDetailXml, "#{applyExcludeTargetProduct}", applyTargetProductXml);
            return promotionDetailXml;
        }

        promotionDetailXml = replace(promotionDetailXml, "#{applyTargetProduct}", applyTargetProductXml);
        promotionDetailXml = delete(promotionDetailXml, "#{applyExcludeTargetProduct}\r\n");
        return promotionDetailXml;
    }

    private List<String> filterNotExistProductCd(List<String> prdCdList) throws Exception {
        String prdSearchRequestXml = FileReadUtil.readAll("salesone_request/productSearchRequest.xml");
        List<String> wrongPrdCd = new ArrayList<>();

        for (String prdCd : prdCdList) {
            prdSearchRequestXml = replace(prdSearchRequestXml, "#{prdCd}", prdCd);
            ResponseEntity<Map> response = HttpUtil.post(prdSearchRequestXml, SalesOnePromotionApiType.POST_SEARCH_PRODUCT.getUrl(), Map.class);

            if (!isExistProductCd(response.getBody())) {
                logger.error("유효하지 않은 상품코드가 요청되었습니다. {}", prdCd);
                throw new ExternalApiFailedException("유효하지 않은 상품코드가 요청되었습니다. " + prdCd);
            }
        }

        return wrongPrdCd;
    }

    private boolean isExistProductCd(Map body) {
        Map dataset = (Map) body.get("dataset");
        if (dataset == null || !dataset.containsKey("record")) {
            return false;
        }

        return true;
    }

    @Override
    public Map<String, String> updatePromotionStatus(String masterSequence, PromotionStatus promotionStatus) throws Exception {
        String changeStatusXml = FileReadUtil.readAll("salesone_request/changeStatus.xml");
        changeStatusXml = replace(changeStatusXml, "#{pmoSeq}", masterSequence);
        changeStatusXml = replace(changeStatusXml, "#{pmoNeoChgVal}", generateRandomVal());
        changeStatusXml = replace(changeStatusXml, "#{pmoNeoStatusCd}", promotionStatus.getStCd());

        ResponseEntity<Map> response = HttpUtil.post(changeStatusXml, SalesOnePromotionApiType.POST_UPDATE_STATUS.getUrl(), Map.class);
        Map<String, String> result = SalesoneUtil.generateApiResult(response, "promotion 상태변경");

        return result;
    }

    private boolean isExceptProduct(PromotionTargetGroup promotionTargetGroup) {
        return "Y".equals(promotionTargetGroup.getPmoApplyExcptYn());
    }

    /**
     * 혜택 적용 배치
     */
    @Override
    public void executeBatch() {
        //배치
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getForObject(BatchApiType.GET_PROMOTION_BATCH.getUrl(), String.class);
    }

    /**
     * 대상상품 조회하여 상품정보 xml 리턴
     *
     * @param promotionDetail
     * @param promotionTargetGroup
     * @return
     * @throws Exception
     */
    private String generateApplyTargetProductXml(PromotionDetail promotionDetail, PromotionTargetGroup promotionTargetGroup) throws Exception {
        List<String> prdCdList = promotionTargetGroup.getPrdCdList();
        String smlClsShopNo = generateRandomVal();
        String prdSearchRequestXml = FileReadUtil.readAll("salesone_request/productSearchRequest.xml");

        StringBuilder applyTargetProductXmlList = new StringBuilder();
        for (String prdCd : prdCdList) {
            prdSearchRequestXml = replace(prdSearchRequestXml, "#{prdCd}", prdCd);
            ResponseEntity<Map> response = HttpUtil.post(prdSearchRequestXml, SalesOnePromotionApiType.POST_SEARCH_PRODUCT.getUrl(), Map.class);
            Map<String, Map<String, Map<String, String>>> body = response.getBody();

            if (SalesoneUtil.isSuccess(body)) {
                Map<String, String> productInfoMap = body.get("dataset").get("record");
                productInfoMap.put("pmoApplyExcptYn", promotionTargetGroup.getPmoApplyExcptYn());
                productInfoMap.put("smlClsShopNo", smlClsShopNo);
                productInfoMap.put("applyStrDtm", promotionDetail.getStartDateFormat());
                productInfoMap.put("applyEndDtm", promotionDetail.getEndDateFormat());
                String applyTargetProductXml = replaceApplyTargetProductXml(prdCd, productInfoMap);
                applyTargetProductXmlList.append(applyTargetProductXml);
            }
        }

        return applyTargetProductXmlList.toString();
    }

    private String replaceApplyTargetProductXml(String prdCd, Map<String, String> productInfoMap) {
        String applyTargetProductXml = FileReadUtil.readAll("salesone_request/applyTargetProduct.xml");
        applyTargetProductXml = replace(applyTargetProductXml, "#{salePrice}", productInfoMap.get("salePrc"));
        applyTargetProductXml = replace(applyTargetProductXml, "#{prchPrice}", productInfoMap.get("supGivRtamt"));
        applyTargetProductXml = replace(applyTargetProductXml, "#{applyStrDtm}", productInfoMap.get("applyStrDtm"));
        applyTargetProductXml = replace(applyTargetProductXml, "#{applyEndDtm}", productInfoMap.get("applyEndDtm"));
        applyTargetProductXml = replace(applyTargetProductXml, "#{prdCd}", prdCd);
        applyTargetProductXml = replace(applyTargetProductXml, "#{prdNm}", productInfoMap.get("prdNm"));
        applyTargetProductXml = replace(applyTargetProductXml, "#{pmoApplyExcptYn}", productInfoMap.get("pmoApplyExcptYn"));
        applyTargetProductXml = replace(applyTargetProductXml, "#{smlClsShopNo}", productInfoMap.get("smlClsShopNo"));

        return applyTargetProductXml;
    }

    private String generateBenefitCardXml(PromotionDetail promotionDetail, PromotionTargetGroup promotionTargetGroup) {
        String benefitCardChargeXml = FileReadUtil.readAll("salesone_request/benefitCardChargeDc.xml");
        benefitCardChargeXml = replace(benefitCardChargeXml, "#{ordCreStrDtm}", promotionDetail.getStartDateFormat());
        benefitCardChargeXml = replace(benefitCardChargeXml, "#{ordCreEndDtm}", promotionDetail.getEndDateFormat());
        benefitCardChargeXml = replace(benefitCardChargeXml, "#{cardAprvStrDtm}", promotionDetail.getStartDateFormat());
        benefitCardChargeXml = replace(benefitCardChargeXml, "#{cardAprvEndDtm}", promotionDetail.getEndDateFormat());
        String cardCoCd = promotionTargetGroup.getCardCoCd();
        benefitCardChargeXml = replace(benefitCardChargeXml, "#{cardCoCd}", cardCoCd);

        for (CardType cardType : CardType.values()) {
            if (cardCoCd.equals(cardType.name())) {
                benefitCardChargeXml = replace(benefitCardChargeXml, "#{ecCardNm}", cardType.getCardName());
            }
        }

        String chrDcRt = promotionTargetGroup.getChrDcRt();
        benefitCardChargeXml = replace(benefitCardChargeXml, "#{chrDcRt}", chrDcRt);
        benefitCardChargeXml = replace(benefitCardChargeXml, "#{dcPsblAmt}", cardCoCd.equals("DA") ? "" : String.valueOf(Integer.parseInt(chrDcRt) * 40000));

        return benefitCardChargeXml;
    }
    private String generateTargetGroup(String pmoSeq, String pmoChgVal) {
        String targetGroupXml = FileReadUtil.readAll("salesone_request/targetGroup.xml");
        targetGroupXml = replace(targetGroupXml, "#{pmoChgVal}", pmoChgVal);
        targetGroupXml = replace(targetGroupXml, "#{pmoSeq}", pmoSeq);
        return targetGroupXml;
    }

    /**
     * 프로모션 Sequence 생성
     *
     * @return Sequence
     */
    @Override
    public Map<String, String> generatePromotionSequence() throws Exception {
        String sessionInfoXml = FileReadUtil.readAll("salesone_request/sessionInfo.xml");
        ResponseEntity<Map> response = HttpUtil.post(sessionInfoXml, SalesOnePromotionApiType.POST_GENERATE_SEQUENCE.getUrl(), Map.class);

        return SalesoneUtil.generateApiResult(response, "sequence 생성");
    }

    @Override
    public String generateRandomVal() {
        Random random = new Random();
        return String.valueOf(random.nextInt(2000000000));
    }
}
