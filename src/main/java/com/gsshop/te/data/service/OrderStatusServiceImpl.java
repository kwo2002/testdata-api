package com.gsshop.te.data.service;

import com.gsshop.te.data.domain.OrderDeliveryInfo;
import com.gsshop.te.data.domain.OrderDeliveryList;
import com.gsshop.te.data.type.BatchApiType;
import com.gsshop.te.data.type.OrderStatusType;
import com.gsshop.te.data.type.SalesOneDlvApiType;
import com.gsshop.te.data.util.FileReadUtil;
import com.gsshop.te.data.util.HttpUtil;
import com.gsshop.te.data.util.SalesoneUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static org.springframework.util.StringUtils.replace;

@Service
public class OrderStatusServiceImpl implements OrderStatusService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void preparingDelivery() throws Exception {
        HttpUtil.get(BatchApiType.DLV_READY_BATCH_1.getUrl());
        HttpUtil.get(BatchApiType.DLV_READY_BATCH_2.getUrl());
    }

    @Override
    public void shipping(String orderNo) throws Exception {
        OrderDeliveryList orderDeliveryList = searchOrder(orderNo, OrderStatusType.PAYMENT_COMPLETED);
        String orderDeliveryInfoXml = saveOrdInfo(orderDeliveryList);
        forceShipping(orderDeliveryInfoXml);
    }

    @Override
    public void deliveryCompleted(String orderNo) throws Exception {
        forceShipCompleted(replaceOrderInfo(searchOrder(orderNo, OrderStatusType.WEHN_SHIPPED)));
    }

    private void forceShipCompleted(String orderDeliveryInfoXml) throws Exception {
        String forceShipmentXml = FileReadUtil.readAll("salesone_request/handleCmpulDlvFsh.xml");
        forceShipmentXml =replace(forceShipmentXml, "#{orderDeliveryInfo}", orderDeliveryInfoXml);

        ResponseEntity<Map> response = HttpUtil.post(forceShipmentXml, SalesOneDlvApiType.POST_FORCE_SHIP_COMPLETED.getUrl(), Map.class);
        SalesoneUtil.generateApiResult(response, "강제배송완료");
    }

    private void forceShipping(String orderDeliveryInfoXml) throws Exception {
        String forceShipmentXml = FileReadUtil.readAll("salesone_request/handleCmpulRelsFsh.xml");
        forceShipmentXml =replace(forceShipmentXml, "#{orderDeliveryInfo}", orderDeliveryInfoXml);

        ResponseEntity<Map> response = HttpUtil.post(forceShipmentXml, SalesOneDlvApiType.POST_FORCE_SHIPPING.getUrl(), Map.class);
        SalesoneUtil.generateApiResult(response, "강제출고");
    }

    private String saveOrdInfo(OrderDeliveryList orderDeliveryList) throws Exception {
        String record = replaceOrderInfo(orderDeliveryList);
        String orderInfoListXml = FileReadUtil.readAll("salesone_request/saveOrdByDstrbInfoModList.xml");
        orderInfoListXml =replace(orderInfoListXml, "#{orderDeliveryInfo}", record);
        ResponseEntity<Map> response = HttpUtil.post(orderInfoListXml, SalesOneDlvApiType.POST_SAVE_ORD.getUrl(), Map.class);
        return record;
    }

    private String replaceOrderInfo(OrderDeliveryList orderDeliveryList) {
        StringBuilder record = new StringBuilder();
        for (OrderDeliveryInfo orderDeliveryInfo : orderDeliveryList.getOrderDeliveryInfoList()) {
            String orderInfoXml = FileReadUtil.readAll("salesone_request/saveOrdByDstrbInfoMod.xml");
            orderInfoXml = replace(orderInfoXml, "#{attrPrdCd}", orderDeliveryInfo.getAttrPrdCd());
            orderInfoXml = replace(orderInfoXml, "#{custAddrZipcd}", orderDeliveryInfo.getCustAddrZipcd());
            orderInfoXml = replace(orderInfoXml, "#{dlvPickMthodDtlCd}", orderDeliveryInfo.getDlvPickMthodDtlCd());
            orderInfoXml = replace(orderInfoXml, "#{dlvPickMthodNm}", orderDeliveryInfo.getDlvPickMthodNm());

            orderInfoXml = replace(orderInfoXml, "#{dtctCd}", orderDeliveryInfo.getDtctCd());
            orderInfoXml = replace(orderInfoXml, "#{ordItemId}", orderDeliveryInfo.getOrdItemId());
            orderInfoXml = replace(orderInfoXml, "#{ordItemNo}", orderDeliveryInfo.getOrdItemNo());
            orderInfoXml = replace(orderInfoXml, "#{ordNo}", orderDeliveryInfo.getOrdNo());
            orderInfoXml = replace(orderInfoXml, "#{ordStCd}", orderDeliveryInfo.getOrdStCd());

            orderInfoXml = replace(orderInfoXml, "#{orgShippntCd}", orderDeliveryInfo.getOrgShippntCd());
            orderInfoXml = replace(orderInfoXml, "#{prchTypCd}", orderDeliveryInfo.getPrchTypCd());
            orderInfoXml = replace(orderInfoXml, "#{prdCd}", orderDeliveryInfo.getPrdCd());
            orderInfoXml = replace(orderInfoXml, "#{prdNm}", orderDeliveryInfo.getPrdNm());
            orderInfoXml = replace(orderInfoXml, "#{relsSchdDt}", orderDeliveryInfo.getRelsSchdDt());
            orderInfoXml = replace(orderInfoXml, "#{shipPickGbnCd}", orderDeliveryInfo.getShipPickGbnCd());
            orderInfoXml = replace(orderInfoXml, "#{shippntCd}", orderDeliveryInfo.getShippntCd());
            orderInfoXml = replace(orderInfoXml, "#{sordItemNo}", orderDeliveryInfo.getSordItemNo());
            orderInfoXml = replace(orderInfoXml, "#{sordNo}", orderDeliveryInfo.getSordNo());
            orderInfoXml = replace(orderInfoXml, "#{sordPriorRankCd}", orderDeliveryInfo.getSordPriorRankCd());
            orderInfoXml = replace(orderInfoXml, "#{sordQty}", orderDeliveryInfo.getSordQty());
            orderInfoXml = replace(orderInfoXml, "#{sordStCd}", orderDeliveryInfo.getSordStCd());
            orderInfoXml = replace(orderInfoXml, "#{supCd}", orderDeliveryInfo.getSupCd());
            orderInfoXml = replace(orderInfoXml, "#{supNm}", orderDeliveryInfo.getSupNm());
            orderInfoXml = replace(orderInfoXml, "#{totalcnt}", orderDeliveryInfo.getTotalcnt());

            record.append(orderInfoXml);
        }
        return record.toString();
    }

    @Override
    public OrderDeliveryList searchOrder(String orderNo, OrderStatusType orderStatus) throws Exception {
        String orderSearchXml = FileReadUtil.readAll("salesone_request/getOrdByDstrbInfoModList.xml");
        orderSearchXml = replace(orderSearchXml, "#{ordNo}", orderNo);
        orderSearchXml = replace(orderSearchXml, "#{stCd}", orderStatus.getStatusCode());

        HttpEntity<String> httpEntity = new HttpEntity<>(orderSearchXml, HttpUtil.generateDefaultHeader());
        return this.restTemplate.postForObject(SalesOneDlvApiType.POST_SEARCH_ORDER.getUrl(), httpEntity, OrderDeliveryList.class);
    }

}
