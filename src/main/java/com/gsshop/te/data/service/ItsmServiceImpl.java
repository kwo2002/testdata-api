package com.gsshop.te.data.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsshop.te.data.constants.RedmineConstants;
import com.gsshop.te.data.domain.ItsmCsr;
import com.gsshop.te.data.domain.ItsmProperties;
import com.gsshop.te.data.repository.ItsmCsrRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.gsshop.te.data.constants.RedmineCustomFieldGenerator.generateField;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Transactional
public class ItsmServiceImpl implements ItsmService {
    private static final Logger logger = LoggerFactory.getLogger(ItsmServiceImpl.class);

    @Autowired
    private ItsmProperties itsmProperties;
    @Autowired
    private ItsmCsrRepository itsmCsrRepository;

    @Override
    public List<ItsmCsr> getCsrList() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<ItsmCsr>> response = restTemplate.exchange("http://" + itsmProperties.getIp() + ":8080/api/itsm/", HttpMethod.GET, null, new ParameterizedTypeReference<List<ItsmCsr>>() {
        });

        List<ItsmCsr> body = response.getBody();
        return body;
    }

    @Override
    public List<ItsmCsr> insertCsrList(List<ItsmCsr> csrList) {
        List<ItsmCsr> savedCsrList = this.itsmCsrRepository.findByReqDtEquals(LocalDate.now());
        List<ItsmCsr> itsmCsrList = csrList.stream().filter(csr -> !savedCsrList.contains(csr)).collect(toList());

        return this.itsmCsrRepository.save(itsmCsrList);
    }

    @Override
    public void saveRedmine(List<ItsmCsr> csrList) {
        for (ItsmCsr itsmCsr : csrList) {
            Map<String, Object> issueJsonMap = getRedmineBasicFieldData(itsmCsr);
            List<Map<String, Object>> customFieldList = getCustomFieldData(itsmCsr);
            issueJsonMap.put("custom_fields", customFieldList);

            Map<String, Map<String, Object>> issueMap = new HashMap<>();
            issueMap.put("issue", issueJsonMap);

            Map<String, List<String>> headerParam = new HashMap<>();
            headerParam.put(RedmineConstants.HEADER_KEY, Arrays.asList(RedmineConstants.API_KEY_ADMIN));
            String issueJson = convertObject2Json(issueMap);
            ResponseEntity<Map> responseEntity =
                    callRestApi(RedmineConstants.REDMINE_URL + RedmineConstants.ISSUE_API, generateHeader(APPLICATION_JSON, headerParam), issueJson);

            if (HttpStatus.CREATED == responseEntity.getStatusCode()) {
                logger.info("Redmine 에 결함이 등록되었습니다.");
                Map<String, Object> responseBody = responseEntity.getBody();

                Map<String, Object> issue = (Map<String, Object>) responseBody.get("issue");
                logger.info("Issue Id: {}", issue.get("id"));
                logger.info("subject(제목): {}", issue.get("subject"));
            } else {
                logger.error("Redmine 에 결함등록 중 오류가 발생하였습니다. Http Status Code: {}", responseEntity.getStatusCodeValue());
            }
        }
    }

    private Map<String, Object> getRedmineBasicFieldData(ItsmCsr itsmCsr) {
        Map<String, Object> map = new HashMap<>();
        map.put("project_id", RedmineConstants.PROJECT_ID);
        map.put("tracker_id", "0083".equals(itsmCsr.getEmerRt()) ? RedmineConstants.TRACKER_EMERGENCY_ID : RedmineConstants.TRACKER_NORMAL_ID);
        map.put("status_id", RedmineConstants.STATUS_ID);
        map.put("priority_id", RedmineConstants.PRIORITY_ID);
        map.put("author_id", RedmineConstants.AUTHOR_ID);
        map.put("subject", itsmCsr.getSrTitlNm());
        map.put("description", itsmCsr.getSrCn());

        return map;
    }

    private List<Map<String, Object>> getCustomFieldData(ItsmCsr itsmCsr) {
        List<Map<String, Object>> customFieldList = new ArrayList<>();

        // CSR NO
        customFieldList.add(generateField(RedmineConstants.CSRNO_FIELD_ID, itsmCsr.getSrId()));

        // 요청분류
        customFieldList.add(generateField(RedmineConstants.CSR_REQ_FIELD_ID, itsmCsr.getReqCl()));

        // 요청자
        customFieldList.add(generateField(RedmineConstants.REQ_USER_FIELD_ID, itsmCsr.getQusrNm()));

        // 개발자
        customFieldList.add(generateField(RedmineConstants.DEVELOPER_FIELD_ID, itsmCsr.getDevelopers()));

        // 모니터링강화여부
        customFieldList.add(generateField(RedmineConstants.MONITORING_FIELD_ID, "Y".equals(itsmCsr.getFncRiskYn()) ? "1": "0"));

        // 업무영역역
        customFieldList.add(generateField(RedmineConstants.SERV_NM, itsmCsr.getServNm()));

        // 배포예정일
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        customFieldList.add(generateField(RedmineConstants.CMPL_AGRE_DT, itsmCsr.getChgCmplAgreDt().format(formatter)));
       return customFieldList;
    }

    /**
     * restapi 를 호출할 때 필요한 Header 값을 생성한다
     *
     * @param mediaType
     * @param headerParam
     * @return
     */
    private HttpHeaders generateHeader(MediaType mediaType, Map<String, List<String>> headerParam) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);
        if (headerParam != null) {
            headers.putAll(headerParam);
        }

        return headers;
    }

    /**
     * rest api 를 호출한다
     *
     * @param url
     * @param headers
     * @param body
     * @return
     */
    private <T> ResponseEntity<Map> callRestApi(String url, HttpHeaders headers, T body) {
        RestTemplate restTemplate = new RestTemplate();
        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        messageConverters.add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        HttpEntity<T> entity = new HttpEntity<>(body, headers);
        return restTemplate.exchange(url, HttpMethod.POST, entity, Map.class);
    }

    public <T> String convertObject2Json(T t) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(t);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
