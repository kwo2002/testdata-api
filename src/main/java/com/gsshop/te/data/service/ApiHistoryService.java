package com.gsshop.te.data.service;

import com.gsshop.te.data.domain.ApiHistory;
import com.gsshop.te.data.domain.ApiList;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface ApiHistoryService {
    void saveHistory(String url, String method, String regUser);

    ApiList selectApi(String url, String method);

    List<ApiHistory> selectApiHistoryList(LocalDateTime begin, LocalDateTime end);

    List<ApiList> selectAllApi();

    List<ApiList> selectApiListRegPeriod(LocalDateTime begin, LocalDateTime end);

    List<Map<String, Object>> selectCallCountPerApi(LocalDateTime begin, LocalDateTime end);

    List<Map<String, Object>> selectBest5Caller();
}
