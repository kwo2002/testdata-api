package com.gsshop.te.data.service;

import com.gsshop.te.data.domain.PromotionDetail;
import com.gsshop.te.data.domain.PromotionMaster;
import com.gsshop.te.data.domain.PromotionTargetGroup;
import com.gsshop.te.data.type.PromotionStatus;

import java.util.List;
import java.util.Map;

public interface PromotionService {

    Map<String, String> saveMaster(PromotionMaster promotionMaster) throws Exception;

    Map<String, String> saveDetail(PromotionDetail promotionDetail) throws Exception;

    List<String> validateProductCd(List<PromotionTargetGroup> promotionTargetGroupList) throws Exception;

    Map<String, String> updatePromotionStatus(String masterSequence, PromotionStatus promotionStatus) throws Exception;

    void executeBatch();

    Map<String, String> generatePromotionSequence() throws Exception;

    String generateRandomVal();
}
