package com.gsshop.te.data.service;

import com.gsshop.te.data.core.exception.ExternalApiFailedException;
import com.gsshop.te.data.dao.ApiHistoryDao;
import com.gsshop.te.data.domain.ApiHistory;
import com.gsshop.te.data.domain.ApiList;
import com.gsshop.te.data.repository.ApiHistoryRepository;
import com.gsshop.te.data.repository.ApiListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class ApiHistoryServiceImpl implements ApiHistoryService {
    @Autowired
    private ApiHistoryRepository apiHistoryRepository;

    @Autowired
    private ApiListRepository apiListRepository;

    @Autowired
    private ApiHistoryDao apiHistoryDao;

    @Transactional
    @Override
    public void saveHistory(String url, String method, String regUser) {
        ApiHistory apiHistory = new ApiHistory();
        apiHistory.setStatus(HttpStatus.CREATED.value());
        apiHistory.setRegDate(LocalDateTime.now());
        apiHistory.setRegUser(regUser);

        ApiList api = this.apiListRepository.findByUrlAndMethod(url, method);
        if (api != null) {
            apiHistory.setApiList(api);
            this.apiHistoryRepository.save(apiHistory);
        }
    }

    @Override
    public ApiList selectApi(String url, String method) {
        return this.apiListRepository.findByUrlAndMethod(url, method);
    }

    @Override
    public List<ApiHistory> selectApiHistoryList(LocalDateTime begin, LocalDateTime end) {
        return this.apiHistoryRepository.findByRegDateBetweenOrderByRegDateDesc(begin, end);
    }



    @Override
    public List<ApiList> selectAllApi() {
        return this.apiListRepository.findAll();
    }

    @Override
    public List<ApiList> selectApiListRegPeriod(LocalDateTime begin, LocalDateTime end) {
        return this.apiListRepository.findByRegDateBetween(begin, end);
    }

    @Override
    public List<Map<String, Object>> selectCallCountPerApi(LocalDateTime begin, LocalDateTime end) {
        return this.apiHistoryDao.selectCallCountPerApi(begin, end);
    }

    @Override
    public List<Map<String, Object>> selectBest5Caller() {
        return this.apiHistoryDao.selectBest5Caller();
    }
}
