package com.gsshop.te.data.service;

import com.gsshop.te.data.domain.OrderDeliveryList;
import com.gsshop.te.data.type.OrderStatusType;

public interface OrderStatusService {
    void preparingDelivery() throws Exception;

    void shipping(String orderNo) throws Exception;

    void deliveryCompleted(String orderNo) throws Exception;

    OrderDeliveryList searchOrder(String orderNo, OrderStatusType orderStatus) throws Exception;
}
