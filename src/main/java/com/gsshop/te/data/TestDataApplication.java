package com.gsshop.te.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableAsync
public class TestDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestDataApplication.class, args);
	}
}
