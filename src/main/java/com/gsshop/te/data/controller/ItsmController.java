package com.gsshop.te.data.controller;

import com.gsshop.te.data.domain.ItsmCsr;
import com.gsshop.te.data.service.ItsmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/itsm")
public class ItsmController {

    @Autowired
    private ItsmService itsmService;

    @GetMapping("/csr")
    public List<ItsmCsr> itsmTest() {
        List<ItsmCsr> csrList = this.itsmService.getCsrList();
        List<ItsmCsr> newCsrList = this.itsmService.insertCsrList(csrList);
        this.itsmService.saveRedmine(newCsrList);
        return newCsrList;
    }

}
