package com.gsshop.te.data.controller;

import com.gsshop.te.data.domain.Promotion;
import com.gsshop.te.data.domain.PromotionDetail;
import com.gsshop.te.data.domain.PromotionMaster;
import com.gsshop.te.data.service.ApiHistoryService;
import com.gsshop.te.data.service.PromotionService;
import com.gsshop.te.data.type.PromotionStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/api/promotion")
@Api(value = "세일즈원 마케팅 프로모션 API", description = "신용카드 청구할인 프로모션",tags = {"프로모션"})
public class PromotionApiController {
    private static final Logger logger = LoggerFactory.getLogger(PromotionApiController.class);

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private ApiHistoryService apiHistoryService;

    private ResponseEntity<Map<String, String>> postPromotionMaster(@ApiParam(name = "제목, 설명") @RequestBody PromotionMaster promotionMaster) throws Exception {
        // 프로모션 sequence 생성
        Map<String, String> seqResult = this.promotionService.generatePromotionSequence();
        promotionMaster.setPmoSeq(seqResult.get("neoPmoSeq"));

        // 프로모션 수정 상태값 생성 (랜덤값)
        String promotionChangeVal = this.promotionService.generateRandomVal();
        promotionMaster.setPmoNeoChgVal(promotionChangeVal);

        // 프로모션 마스터 저장
        Map<String, String> result = this.promotionService.saveMaster(promotionMaster);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    private ResponseEntity<Map<String, String>> postPromotionDetail(@ApiParam(name = "고객그룹, 혜택정보, 대상상품") @RequestBody PromotionDetail promotionDetail) throws Exception {
        // 프로모션 상세정보 저장
        Map<String, String> result = this.promotionService.saveDetail(promotionDetail);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @ApiOperation(value = "신용카드 청구할인 프로모션 생성", notes = "마케팅 프로모션 저장, 고객그룹/혜택/대상상품 저장, 프로모션 승인, 프로모션 배치")
    @PostMapping("/")
    public void postPromotion(@ApiParam(name = "프로모션 데이터") @RequestBody Promotion promotion) throws Exception {
        // 프로모션 마스터 저장
        PromotionMaster promotionMaster = promotion.getPromotionMaster();
        ResponseEntity<Map<String, String>> masterResponse = postPromotionMaster(promotionMaster);
        Map<String, String> masterResponseBody = masterResponse.getBody();

        // 프로모션 상세에 저장된 마스터 정보 (sequence, changeVal) 셋팅
        PromotionDetail promotionDetail = promotion.getPromotionDetail();
        String pmoSeq = masterResponseBody.get("pmoSeq");
        promotionDetail.setPmoSeq(pmoSeq);
        promotionDetail.setPmoChgVal(masterResponseBody.get("pmoChgVal"));
        promotionDetail.setStartDt(promotionMaster.getStartDate());
        promotionDetail.setEndDt(promotionMaster.getEndDate());

        // 프로모션 상세 (고객그룹/혜택/대상상품) 저장
        postPromotionDetail(promotionDetail);

        // 프로모션 상태 업데이트 : [등록]승인요청
        this.promotionService.updatePromotionStatus(pmoSeq, PromotionStatus.REQUEST);
        // 프로모션 상태 업데이트 : [등록]승인완료
        this.promotionService.updatePromotionStatus(pmoSeq, PromotionStatus.APPROVAL);
        // 배치 수행
        this.promotionService.executeBatch();

        this.apiHistoryService.saveHistory("/te/api/promotion", HttpMethod.POST.name(), promotion.getRegUser());
    }

    @ApiOperation(value = "신용카드 청구할인 프로모션 종료", notes = "[등록]승인완료(진행중) -> [수정]반려(시작전)")
    @DeleteMapping("/{pmoSeq}")
    public void endPromotion(@ApiParam(value = "프로모션 번호", required = true) @PathVariable("pmoSeq") String pmoSeq, @ApiParam(value = "등록자(사번)", required = true, example = "D2714") @RequestBody String regUser) throws Exception {
        // 프로모션 상태 업데이트 : [등록]승인후수정 - [수정]승인요청 - [등록]반려
        this.promotionService.updatePromotionStatus(pmoSeq,PromotionStatus.MODIFY);
        this.promotionService.updatePromotionStatus(pmoSeq,PromotionStatus.MODIFY_REQUEST);
        Map<String, String> result = this.promotionService.updatePromotionStatus(pmoSeq, PromotionStatus.REJECT);
        result.put("regUser", regUser);

        // 배치 수행
        this.promotionService.executeBatch();

        this.apiHistoryService.saveHistory("/te/api/promotion", HttpMethod.DELETE.name(), regUser);
    }

}
