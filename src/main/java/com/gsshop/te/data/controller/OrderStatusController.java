package com.gsshop.te.data.controller;

import com.gsshop.te.data.service.ApiHistoryService;
import com.gsshop.te.data.service.OrderStatusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/order/status")
@Api(value = "주문 상태변경 API", description = "배송준비중, 상품배송중, 배송완료",tags = {"주문상태변경"})
public class OrderStatusController {
    private static final Logger logger = LoggerFactory.getLogger(OrderStatusController.class);

    @Autowired
    private OrderStatusService orderStatusService;

    @Autowired
    private ApiHistoryService apiHistoryService;

    @ApiOperation(value = "배송준비중", notes = "주문 상태를 '배송준비중' 으로 변경")
    @PutMapping("/preparing_delivery")
    public void preparingDelivery(@ApiParam(value = "등록자(사번)", required = true, example = "D2714") @RequestParam String regUser) throws Exception {
        this.orderStatusService.preparingDelivery();
        this.apiHistoryService.saveHistory("/api/order/status/preparing_delivery", HttpMethod.PUT.name(), regUser);
    }

    @ApiOperation(value = "상품배송중", notes = "주문 상태를 '상품배송중' 으로 변경", position = 1)
    @PutMapping("/shipping")
    public void shipping(@ApiParam(value = "주문번호", required = true) @RequestParam String orderNo, @ApiParam(value = "등록자(사번)", required = true, example = "D2714") @RequestParam String regUser) throws Exception {
        this.orderStatusService.preparingDelivery();
        this.orderStatusService.shipping(orderNo);

        this.apiHistoryService.saveHistory("/te/api/order/status/shipping", HttpMethod.PUT.name(), regUser);
    }

    @ApiOperation(value = "배송완료", notes = "주문 상태를 '배송완료' 로 변경", position = 2)
    @PutMapping("/delivery_completed")
    public void deliveryCompleted(@ApiParam(value = "주문번호", required = true) @RequestParam String orderNo, @ApiParam(value = "등록자(사번)", required = true, example = "D2714") @RequestParam String regUser) throws Exception {
        this.orderStatusService.preparingDelivery();
        this.orderStatusService.shipping(orderNo);
        this.orderStatusService.deliveryCompleted(orderNo);

        this.apiHistoryService.saveHistory("/te/api/order/status/delivery_completed", HttpMethod.PUT.name(), regUser);
    }


}
