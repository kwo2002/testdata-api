package com.gsshop.te.data.controller;

import com.gsshop.te.data.domain.ApiHistory;
import com.gsshop.te.data.domain.ApiList;
import com.gsshop.te.data.service.ApiHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/history")
@ApiIgnore
public class ApiHistoryController {

    @Autowired
    private ApiHistoryService apiHistoryService;

    @GetMapping("/callapi")
    public String apiHistory(Model model) {
        LocalDateTime end = LocalDateTime.now();
        LocalDateTime begin = end.minusWeeks(8);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        List<ApiHistory> apiHistoryList = this.apiHistoryService.selectApiHistoryList(begin, end);
        List<ApiList> apiList = this.apiHistoryService.selectAllApi();
        List<ApiList> recentApi = this.apiHistoryService.selectApiListRegPeriod(end.minusWeeks(2), end);
        List<Map<String, Object>> best5Caller = this.apiHistoryService.selectBest5Caller();
        List<Map<String, Object>> countPerApi = this.apiHistoryService.selectCallCountPerApi(begin, end);

        long successCount = apiHistoryList.stream().filter(a -> 200 == a.getStatus() || 201 == a.getStatus()).count();
        long failedCount = apiHistoryList.size() - successCount;

        model.addAttribute("dtToday", end.format(formatter));
        model.addAttribute("dtBeforeWeek", begin.format(formatter));
        model.addAttribute("apiList", apiList);
        model.addAttribute("successCount", successCount);
        model.addAttribute("failedCount", failedCount);
        model.addAttribute("totalCount", apiHistoryList.size());
        model.addAttribute("apiListPeriod", recentApi);
        model.addAttribute("best5Caller", best5Caller);
        model.addAttribute("apiHistory", apiHistoryList);
        model.addAttribute("countPerApi", countPerApi);

        return "history/week_api_history";
    }

}
