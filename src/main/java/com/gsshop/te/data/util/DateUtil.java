package com.gsshop.te.data.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {
    public static String today() {
        return today(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    public static String today(DateTimeFormatter formatter) {
        LocalDateTime now = LocalDateTime.now();
        return now.format(formatter);
    }

    public static String todayPlusDays(int day) {
        return todayPlusDays(day, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    public static String todayPlusWeeks(int week) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime localDateTime = now.plusWeeks(week);
        return localDateTime.format(formatter);
    }

    public static String todayPlusDays(int day, DateTimeFormatter formatter) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime localDateTime = now.plusDays(day);
        return localDateTime.format(formatter);
    }
}
