package com.gsshop.te.data.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReadUtil {
    private static final Logger logger = LoggerFactory.getLogger(FileReadUtil.class);

    public static String readAll(String fileName) {
        return readAll(fileName, "src/main/resources/");
    }

    public static String readAll(String fileName, String path) {
        try {
            return new String(Files.readAllBytes(Paths.get(path + fileName)), "utf-8");
        } catch (IOException e) {
            logger.error("{} 이 존재하지 않습니다.", fileName);
            return "";
        }
    }

}
