package com.gsshop.te.data.util;

import com.gsshop.te.data.core.exception.ExternalApiCallException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class HttpUtil {
    private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    public static HttpHeaders generateDefaultHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();

        List<Charset> acceptCharsetList = new ArrayList<>();
        acceptCharsetList.add(Charset.forName("utf-8"));

        httpHeaders.setAcceptCharset(acceptCharsetList);
        httpHeaders.set("Accept-Language", "ko-KR");
        httpHeaders.set("Content-Type", "text/xml;charset=utf-8");
        httpHeaders.setCacheControl("no-cache");
        httpHeaders.set("Accept", "*/*");

        return httpHeaders;

    }

    public static ResponseEntity<String> post(String postBody, String path) throws Exception {
        return post(postBody, path, String.class);
    }

    public static <T> ResponseEntity<T> post(String postBody, String path, Class<T> clazz) throws Exception {
        HttpEntity<String> httpEntity = new HttpEntity<>(postBody, generateDefaultHeader());
        RestTemplate restTemplate = new RestTemplate();

        try {
            return restTemplate.postForEntity(path, httpEntity, clazz);
        } catch (RestClientException e) {
            logger.error("API 호출 중 오류가 발생하였습니다. URL: {}", path);
            logger.error("Error Message: {}", e.getMessage());
            throw new ExternalApiCallException("외부 API 서버의 호출에 실패하였습니다. URL: " + path);
        }
    }

    public static String get(String path) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.getForObject(path, String.class);
        } catch (RestClientException e) {
            logger.error("API 호출 중 오류가 발생하였습니다. URL: {}", path);
            logger.error("Error Message: {}", e.getMessage());
            throw new ExternalApiCallException("외부 API 서버의 호출에 실패하였습니다. URL: " + path);
        }
    }
}
