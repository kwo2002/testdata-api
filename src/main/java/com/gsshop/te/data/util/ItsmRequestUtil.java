package com.gsshop.te.data.util;

import java.util.HashMap;
import java.util.Map;

public class ItsmRequestUtil {
    private static Map<String, String> reqClMap = new HashMap<>();

    private static void generateReqClassification() {
        reqClMap.put("0663", "대상 : 신규개발-프로세스 변경 유");
        reqClMap.put("0664", "대상 : 신규개발-프로세스 변경 무");
        reqClMap.put("0640", "대상 : 신규개발-기능,프로세스추가요청");
        reqClMap.put("0638", "대상 : 기존기능개선-정보추가/변경/제거요청");
        reqClMap.put("0665", "대상 : 기존기능개선-프로세스변경 유");
        reqClMap.put("0666", "대상 : 기존기능개선-프로세스변경 무");
        reqClMap.put("0191", "비대상 : 비기능 변경-단순 Display변경");
        reqClMap.put("0661", "비대상 : 쿼리튜닝(성능), batch");
    }

    public static String getReqClValue(String reqCl) {
        generateReqClassification();
        return reqClMap.get(reqCl);
    }
}
