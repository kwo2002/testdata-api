package com.gsshop.te.data.util;

import com.gsshop.te.data.core.exception.ExternalApiFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class SalesoneUtil {
    private static final Logger logger = LoggerFactory.getLogger(SalesoneUtil.class);

    public static boolean isSuccess(Map<String, Map<String, Map<String, String>>> responseBody) {
        String result = responseBody.get("params").get("param").get("");
        // ErrorCode = 0 이면 success
        return !StringUtils.isEmpty(result) && "0".equals(result);
    }

    public static Map<String, String> generateApiResult(ResponseEntity<Map> response, String message) {
        Map<String, Map<String, Map<String, String>>> body = response.getBody();

        if (!isSuccess(body)) {
            logger.error("{}에 실패 하였습니다.", message);
            logger.error("세일즈원 {} 실패 응답메세지(Response Body)는 다음과 같습니다. {}", message, body);
            throw new ExternalApiFailedException(message + "에 실패 하였습니다.");
        }

        logger.info("{} 에 성공 하였습니다.", message);
        logger.info("세일즈원 {} 응답메세지(Response Body)는 다음과 같습니다. {}", message, body);

        Map<String, String> result = new HashMap<>();
        if (body.containsKey("dataset")) {
            result = body.get("dataset").get("record");
        }
        result.put("message", message + "에 성공 하였습니다.");
        result.put("status", "201");
        return result;
    }
}
