<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>API 히스토리</title>
    <script src="<spring:url value="/js/jquery-3.2.1.min.js"/>"></script>
    <link href="<spring:url value="/css/testautomation.css"/>" rel="stylesheet">
    <link href="<spring:url value="/css/bootstrap.css"/>" rel="stylesheet">
    <script src="<spring:url value="/js/Chart.js"/>"></script>
    <script src="<spring:url value="/js/kpi_util.js"/>"></script>
    <script>
        $(document).ready(function(){
            var canvas = document.getElementById('cnv-top5');
            var callers = [];
            var counts = [];
            <c:forEach items="${best5Caller}" var="caller">
                callers.push('${caller.reg_user}');
                counts.push(${caller.cnt});
            </c:forEach>
            var data = {
                labels: callers,
                datasets: [
                    {
                        label: "API 호출 랭커",
                        borderWidth: 2,
                        hoverBackgroundColor: "rgba(255,99,132,0.4)",
                        hoverBorderColor: "rgba(255,99,132,1)",
                        data: counts,
                    }
                ]
            };
            var option = {
                scales: {
                    yAxes:[{
                        stacked:true,
                        gridLines: {
                            display:true
                        }
                    }],
                    xAxes:[{
                        gridLines: {
                            display:false
                        }
                    }]
                }
            };

            var myBarChart = Chart.Bar(canvas,{
                data:data,
                options:option
            });
        });
    </script>
    </head>
<body>

<div class="container">
    <div class="well well-sm"><h1><span id="span_weekly"></span></h1></div>
    <div class="row">
        <div class="col-md-3">
            <div class="x_panel">
                <div class="x_title">
                    <h4>API 호출 횟수</h4>
                </div>
                <div class="vertical-align" style="height: 189px;color: darkred;">
                    <span style="font-size: 55pt;">&nbsp;&nbsp; ${totalCount}</span>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>API 호출 TOP 3</h4>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>제목</th>
                            <th>호출횟수</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${countPerApi}" var="topApi">
                            <tr>
                                <td>${topApi.name}</td>
                                <td>${topApi.count}</td>
                            </tr>
                        </c:forEach>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>API 최근 호출 기록</h4>
                    <div class="clearfix"></div>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th>제목</th>
                        <th>호출자</th>
                        <th>호출일</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${apiHistory}" var="apiHistory" begin="0" end="5">
                        <tr>
                            <td>
                                    ${apiHistory.apiList.name}
                            </td>
                            <td>
                                    ${apiHistory.regUser}
                            </td>
                            <td>
                                    ${apiHistory.regDate}
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>API 호출 유저 TOP5</h4>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <canvas id="cnv-top5"></canvas>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>신규 API</h4>
                    <div class="clearfix"></div>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th>번호</th>
                        <th>Method</th>
                        <th>이름</th>
                    </tr>
                    </thead>
                    <tbody id="tr_testcase">
                    <c:forEach items="${apiListPeriod}" var="api">
                        <tr>
                            <td>
                                    ${api.seq}
                            </td>
                            <td>
                                    ${api.method}
                            </td>
                            <td>
                                    ${api.description}
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4>전체 API</h4>
                    <div class="clearfix"></div>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th>번호</th>
                        <th>Method</th>
                        <th>URL</th>
                        <th>설명</th>
                    </tr>
                    </thead>
                    <tbody id="tr_allcase">
                    <c:forEach items="${apiList}" var="api">
                        <tr>
                            <td>
                                ${api.seq}
                            </td>
                            <td>
                                ${api.method}
                            </td>
                            <td>
                                ${api.url}
                            </td>
                            <td>
                                ${api.description}
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
</body>
</html>
