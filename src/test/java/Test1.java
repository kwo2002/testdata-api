import com.gsshop.te.data.constants.RedmineConstants;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class Test1 {
    public static void main(String[] args) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add(RedmineConstants.HEADER_KEY, RedmineConstants.API_KEY_ADMIN);
        HttpEntity httpEntity = new HttpEntity<>(headers);
        ResponseEntity<Map> entity = restTemplate.exchange(RedmineConstants.REDMINE_URL + "/projects/itsm/memberships.json?limit=200", HttpMethod.GET, httpEntity, Map.class);
        System.out.println(entity.getBody());
    }

}
