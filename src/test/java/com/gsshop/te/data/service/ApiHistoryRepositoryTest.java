package com.gsshop.te.data.service;

import com.gsshop.te.data.dao.ApiHistoryDao;
import com.gsshop.te.data.repository.ApiHistoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ApiHistoryRepositoryTest {
    @Autowired
    private ApiHistoryDao apiHistoryDao;


    @Test
    public void name() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        List<Map<String, Object>> maps = this.apiHistoryDao.selectCallCountPerApi(now.minusWeeks(1), now);
        System.out.println(maps);
    }
}
